import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TrainingComponent } from './training/training.component';
import { PostComponent } from './post/post.component';
import { HomeComponent } from './home/home.component';
import {MDBBootstrapModule} from 'angular-bootstrap-md';
// import { NewTrainingComponent } from './training/new-training/new-training.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {NgbAlertModule, NgbModule, NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule} from '@angular/forms';
import { ExerciseComponent } from './training/exercise/exercise.component';
import { ExerciseRecordComponent } from './training/exercise-record/exercise-record.component';
import { GraphicsComponent } from './training/graphics/graphics.component';

@NgModule({
  declarations: [
    AppComponent,
    TrainingComponent,
    PostComponent,
    HomeComponent,
    // NewTrainingComponent,
    ExerciseComponent,
    ExerciseRecordComponent,
    GraphicsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MDBBootstrapModule.forRoot(),
    HttpClientModule,
    NgbModule,
    NgbPaginationModule,
    NgbAlertModule,
    FormsModule
  ],
  providers: [
    // { provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'fill' } }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
