export class ExerciseRecord {
  id?: number;
  exerciseId: number;
  weight: number;
  quantity: number;
  date: Date;

  constructor(id?: number, exerciseId?: number, weight?: number, quantity?: number, date?: Date) {
    this.id = id;
    this.exerciseId = exerciseId;
    this.weight = weight;
    this.quantity = quantity;
    this.date = date;
  }
}

export class Exercise {
  id?: number;
  name: string;

  constructor(id?: number, name?: string) {
    this.id = id;
    this.name = name;
  }
}

