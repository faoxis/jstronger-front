export class RodLiftsNumberStatisticPerWeek {
  exerciseId: number;
  date: string;
  value: number;

  constructor(exerciseId: number, date: string, value: number) {
    this.exerciseId = exerciseId;
    this.date = date;
    this.value = value;
  }
}
