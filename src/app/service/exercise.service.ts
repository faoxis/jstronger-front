import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Exercise} from '../model/exercise.model';
import {Observable, of} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ExerciseService {

  private url = '';
  private headers = new HttpHeaders({ 'Content-Type': 'application/json' });

  private exercises: Observable<Exercise> =
    of(new Exercise(1, 'her'), new Exercise(2, 'something else'), new Exercise(3, 'third'))
  private counter = 4;

  constructor(private httpClient: HttpClient) {
  }

  getAllExercises(): Observable<Exercise> {
    // return new Observable((observable: Observable<Exercise>, _) => {
    //
    //   observable.complete();
    //
    //   return unsubscribe() {};
    // });
    return this.exercises;

    // return this.httpClient
    //   .get<Exercise[]>(this.url, { headers: this.headers });
  }

  save(exercise: Exercise): Observable<Exercise> {
    const exersicesList: Exercise[] = []
    this.exercises.subscribe(exercise => {
      exersicesList.unshift(exercise)
    })
    exercise.id = this.counter++;
    exersicesList.unshift(exercise);

    this.exercises = of(...exersicesList);
    return of(exercise);
  }

}
