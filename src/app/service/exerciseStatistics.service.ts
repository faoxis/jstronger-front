import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {RodLiftsNumberStatisticPerWeek} from '../model/exerciseStatistics.model';

@Injectable({
  providedIn: 'root'
})
export class ExerciseStatisticsService {
  private url = '';
  private headers = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private httpClient: HttpClient) { }

  public getRodLiftsNumber(exerciseId): Observable<RodLiftsNumberStatisticPerWeek> {
    if (exerciseId == 1) {
      return of(
        new RodLiftsNumberStatisticPerWeek(1, '2021-01-01', 23),
        new RodLiftsNumberStatisticPerWeek(1, '2021-01-05', 22),
        new RodLiftsNumberStatisticPerWeek(1, '2021-01-10', 23),
        new RodLiftsNumberStatisticPerWeek(1, '2021-01-15', 53),
        new RodLiftsNumberStatisticPerWeek(1, '2021-01-20', 53),
        new RodLiftsNumberStatisticPerWeek(1, '2021-01-25', 76),
        new RodLiftsNumberStatisticPerWeek(1, '2021-01-30', 32),
        new RodLiftsNumberStatisticPerWeek(1, '2021-02-01', 57),
        new RodLiftsNumberStatisticPerWeek(1, '2021-02-05', 23),
        new RodLiftsNumberStatisticPerWeek(1, '2021-02-10', 34)
      );
    } else {
      return of(
        new RodLiftsNumberStatisticPerWeek(2, '2021-01-01', 22),
        new RodLiftsNumberStatisticPerWeek(2, '2021-01-05', 24),
        new RodLiftsNumberStatisticPerWeek(2, '2021-01-10', 42),
        new RodLiftsNumberStatisticPerWeek(2, '2021-01-15', 42),
        new RodLiftsNumberStatisticPerWeek(2, '2021-01-20', 51),
        new RodLiftsNumberStatisticPerWeek(2, '2021-01-25', 12),
        new RodLiftsNumberStatisticPerWeek(2, '2021-01-30', 91),
        new RodLiftsNumberStatisticPerWeek(2, '2021-02-01', 13),
        new RodLiftsNumberStatisticPerWeek(2, '2021-01-05', 64),
        new RodLiftsNumberStatisticPerWeek(2, '2021-01-04', 42),
      );
    }
  }

}
