import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ExerciseRecord} from '../model/exercise.model';
import {Observable, of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ExerciseRecordService {
  private url = '';
  private headers = new HttpHeaders({ 'Content-Type': 'application/json' });

  private exerciseRecords: ExerciseRecord[] = [];
  private count = 0;

  constructor(private httpClient: HttpClient) { }

  save(record: ExerciseRecord): Observable<ExerciseRecord> {
    this.exerciseRecords.push(record);
    record.id = this.count++;
    return of(record);
  }

  getLast(count: number): Observable<ExerciseRecord> {
    return of(...this.exerciseRecords);
  }

}
