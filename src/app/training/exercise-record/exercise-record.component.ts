import {Component, Input, OnInit} from '@angular/core';
import {ExerciseRecordService} from '../../service/exerciseRecord.service';
import {Exercise, ExerciseRecord} from '../../model/exercise.model';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-exercise-record',
  templateUrl: './exercise-record.component.html',
  styleUrls: ['./exercise-record.component.scss']
})
export class ExerciseRecordComponent implements OnInit {

  model: NgbDateStruct;
  records: ExerciseRecord[] = [];
  @Input() exercises: Exercise[];

  constructor(private service: ExerciseRecordService) {
    this.updateExerciseRecordList();
  }

  ngOnInit(): void {
  }

  findExerciseNameById(id: number): string {
    return this.exercises.filter((exercise, _) => exercise.id == id )[0].name;
  }

  addNewExerciseRecord(exerciseId: number, weight: number, repetitionsNumber: number, date: string): void {
    const record = new ExerciseRecord(null, exerciseId, weight, repetitionsNumber, new Date(date));
    this.service.save(record);
    this.updateExerciseRecordList();
  }

  private updateExerciseRecordList(): void {
    this.records = [];
    this.service.getLast(10).subscribe(record => {
      this.records.unshift(record);
    });
  }

}
