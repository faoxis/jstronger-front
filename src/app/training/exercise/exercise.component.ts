import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {ExerciseService} from '../../service/exercise.service';
import {Exercise} from '../../model/exercise.model';

@Component({
  selector: 'app-exercise',
  templateUrl: './exercise.component.html',
  styleUrls: ['./exercise.component.scss']
})
export class ExerciseComponent implements OnInit {

  exercises: Exercise[] = [];
  newExercise: Exercise = new Exercise();
  @Output() exerciseUpdateEvent = new EventEmitter<Exercise[]>();

  constructor(private exerciseService: ExerciseService) {
  }

  ngOnInit(): void {
    this.updateExerciseList();
  }

  addNewExercise(exercise: string): void {
    this.exerciseService.save(new Exercise(null, exercise)).subscribe(_ => {
      this.updateExerciseList();
    });
    console.log(exercise);
  }

  private updateExerciseList(): void {
    this.exercises = [];
    this.exerciseService.getAllExercises().subscribe(exercise => {
      this.exercises.unshift(exercise);
      this.exerciseUpdateEvent.emit(this.exercises);
    });
  }

}
