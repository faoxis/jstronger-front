import { Component, OnInit } from '@angular/core';
import {Exercise, ExerciseRecord} from '../model/exercise.model';

@Component({
  selector: 'app-training',
  templateUrl: './training.component.html',
  styleUrls: ['./training.component.scss']
})
export class TrainingComponent implements OnInit {
  exercises: Exercise[] = [];
  constructor() {
  }

  ngOnInit(): void {

  }

  public updateExercises(exercises: Exercise[]): void {
    this.exercises = exercises;
  }

}
