import {Component, Input, OnInit} from '@angular/core';
import {Exercise} from '../../model/exercise.model';
import {ExerciseStatisticsService} from '../../service/exerciseStatistics.service';
import {RodLiftsNumberStatisticPerWeek} from '../../model/exerciseStatistics.model';

@Component({
  selector: 'app-graphics',
  templateUrl: './graphics.component.html',
  styleUrls: ['./graphics.component.scss']
})
export class GraphicsComponent implements OnInit {

  public chartType = 'line';
  @Input() exercises: Exercise[];
  rodLiftsStatistics: RodLiftsNumberStatisticPerWeek[] = [];

  public chartDatasets: Array<any> = [
    // { data: [{ x: 1, y: 1}, { x: 2, y: 2}, { x: 3, y: 3}, { x: 4, y: 4}, { x: 5, y: 5}, { x: 6, y: 6}, { x: 7, y: 7}], label: 'My First dataset' },
    // { data: [28, 48, 40, 19, 86, 27, 90], label: 'My Second dataset' }
  ];

  public chartLabels: Array<any> = [];

  // public chartColors: Array<any> = [
  //   {
  //     backgroundColor: 'rgba(105, 0, 132, .2)',
  //     borderColor: 'rgba(200, 99, 132, .7)',
  //     borderWidth: 2,
  //   },
  //   {
  //     backgroundColor: 'rgba(0, 137, 132, .2)',
  //     borderColor: 'rgba(0, 10, 130, .7)',
  //     borderWidth: 2,
  //   }
  // ];

  public chartOptions: any = {
    responsive: true
  };
  public chartClicked(e: any): void { }
  public chartHovered(e: any): void { }


  constructor(private service: ExerciseStatisticsService) {
    console.log('In graphics: ');
    console.log(this.exercises);
  }

  ngOnInit(): void {
    console.log(this.exercises);
    // this.getRodLiftsNumberPerWeek();
  }

  // getRodLiftsNumberPerWeek(): void {
    // this.service.getRodLiftsNumberPerWeek().subscribe(statistics => {
    //   this.rodLiftsStatistics.push(statistics);
    //   this.updateStatisticsChard();
    // });
  // }


  // private updateStatisticsChard(): void {
  //   if (this.exercises == null || this.exercises.length == 0) {
  //     console.log('in update statistics chard');
  //     return;
  //   }
  //
  //   const grouppedStatistics = new Map<string, number[]>();
  //   console.log('groupedStatistics has been created!');
  //   this.rodLiftsStatistics.forEach(statistic => {
  //     console.log(this.exercises);
  //     console.log(statistic);
  //     const exerciseName = this.findExerciseNameById(statistic.exerciseId);
  //     console.log('before if');
  //     if (!grouppedStatistics.has(exerciseName)) {
  //       grouppedStatistics.set(exerciseName, []);
  //     }
  //     console.log('after if');
  //     grouppedStatistics.get(exerciseName).push(statistic.value);
  //   });
  //   this.chartDatasets = [];
  //   console.log('grouppedStatistics: ');
  //   console.log(grouppedStatistics);
  //   console.log('before for each');
  //   console.log('size: ' + grouppedStatistics.size);
  //   console.log(grouppedStatistics.keys());
  //   console.log(grouppedStatistics.values());
  //   console.log(grouppedStatistics[Symbol.iterator]());
    // grouppedStatistics.forEach((value, key) => console.log(value));
    // for (const exercise of groupedStatistics.keys()) {
    //   this.chartDatasets.push({
    //     label: exercise, data: groupedStatistics[exercise]
    //   });
    // }
    // grouppedStatistics.forEach((values, exercise) => {
    //   console.log('in for each');
    //   console.log('values');
    //   console.log(values);
    //   this.chartDatasets.push({
    //     label: exercise, data: values
    //   });
    // });
    // console.log('after for each');
    // console.log(this.chartDatasets);
    // this.chartLabels = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'];
  // }

  private findExerciseNameById(id: number): string {
    console.log('id: ' + id);
    return this.exercises.filter((exercise, _) => exercise.id == id )[0].name;
  }


  buildRodLiftsNumberChart(exerciseId: number): void {
    const datasets: number[] = [];
    const labels: string[] = [];
    const exerciseName = this.findExerciseNameById(exerciseId);
    this.service.getRodLiftsNumber(exerciseId).subscribe(statistic => {
      datasets.push(statistic.value);
      labels.push(statistic.date);
      this.chartDatasets = [{data: datasets, label: 'КПШ'}];
      this.chartLabels = labels;
    });
  }
}
