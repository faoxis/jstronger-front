import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {TrainingComponent} from './training/training.component';
import {PostComponent} from './post/post.component';
import {AppComponent} from './app.component';
import {HomeComponent} from './home/home.component';
// import {NewTrainingComponent} from './training/new-training/new-training.component';
// import { StrongComponent } from 'app/strong/strong.component.ts'

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'training', component: TrainingComponent },
  // { path: 'training/new-training', component: NewTrainingComponent },
  { path: 'post', component: PostComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
